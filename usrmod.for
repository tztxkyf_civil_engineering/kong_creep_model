      Subroutine User_Mod ( IDTask, iMod, IsUndr,
     *                      iStep, iTer, iEl, Int,
     *                      X, Y, Z,
     *                      Time0, dTime,
     *                      Props, Sig0, Swp0, StVar0,
     *                      dEps, D, BulkW,
     *                      Sig, Swp, StVar, ipl,
     *                      nStat, NonSym, iStrsDep, iTimeDep,iTang,
     *                      iPrjDir, iPrjLen, iAbort )
!
! Purpose: User supplied soil model
!          Example: iMod=1 : KYF003 !一种简单的粘弹-粘塑性模型。基于MC模型的模型。
!                   iMod=2 : 
!
!  Depending on IDTask, 1 : Initialize state variables
!                       2 : calculate stresses,
!                       3 : calculate material stiffness matrix
!                       4 : return number of state variables
!                       5 : inquire matrix properties
!                           return switch for non-symmetric D-matrix
!                           stress/time dependent matrix
!                       6 : calculate elastic material stiffness matrix
! Arguments:
!          I/O  Type
!  IDTask   I   I    : see above
!  iMod     I   I    : model number (1..10)
!  IsUndr   I   I    : =1 for undrained, 0 otherwise
!  iStep    I   I    : Global step number
!  iter     I   I    : Global iteration number
!  iel      I   I    : Global element number
!  Int      I   I    : Global integration point number
!  X        I   R    : X-Position of integration point
!  Y        I   R    : Y-Position of integration point
!  Z        I   R    : Z-Position of integration point
!  Time0    I   R    : Time at start of step
!  dTime    I   R    : Time increment
!  Props    I   R()  : List with model parameters
!  Sig0     I   R()  : Stresses at start of step
!  Swp0     I   R    : Excess pore pressure start of step
!  StVar0   I   R()  : State variable at start of step
!  dEps     I   R()  : Strain increment
!  D       I/O  R(,) : Material stiffness matrix
!  BulkW   I/O  R    : Bulkmodulus for water (undrained only)
!  Sig      O   R()  : Resulting stresses
!  Swp      O   R    : Resulting excess pore pressure
!  StVar    O   R()  : Resulting values state variables
!  ipl      O   I    : Plasticity indicator
!  nStat    O   I    : Number of state variables
!  NonSym   O   I    : Non-Symmetric D-matrix ?
!  iStrsDep O   I    : =1 for stress dependent D-matrix
!  iTimeDep O   I    : =1 for time dependent D-matrix
!  iTang    O   I    : =1 for tangent matrix
!  iAbort   O   I    : =1 to force stopping of calculation
!
      Implicit Double Precision (A-H, O-Z)
!
      Dimension Props(*), Sig0(*), StVar0(*), dEps(*), D(6,6),
     *          Sig(*),   StVar(*), iPrjDir(*)
      Character*255 PrjDir, Dbg_Name
      Data iounit / 0 /
      Save iounit
!
!---  Local variables
!

!      Logical IsOpen

! Next line should be used for DLL-option (DF compiler)
!!! !DEC$ ATTRIBUTES DLLExport, StdCall, Reference :: User_Mod
! Next line should be used for DLL-option (LF compiler)
!!!      DLL_Export User_Mod
      include 'impexp'
      ! Possibly open a file for debugging purposes
      If (iounit.Eq.0) Then
        PrjDir=' '
        Do i=1,iPrjLen
          PrjDir(i:i) = Char( iPrjDir(i) )
        End Do

        Dbg_Name=PrjDir(:iPrjLen)//'udsmex'
        nErr=0
    1   Continue
          Open( Unit= 1, File= Dbg_Name,iostat=ios)
          If (ios.Eq.0) Close(Unit=1,Status='delete',iostat=ios)

          If (ios.Ne.0) Then
            !
            ! in case of error try ...udsmex1 or udsmex2 or ..
            !
            nErr=nErr+1
            dbg_name=PrjDir(:iPrjLen)//'udsmex'//char(48+nErr)

            If (nErr.Lt.10) Goto 1
          End If

        Open( Unit= 1, File= Dbg_Name,blocksize=4096)

        Write(1,*)'File 1 opened'
        iounit = 1
        Call WriVec(1,'Props',Props,100)
      End If
      Call WriIvl( -1, 'iounit',iounit )
      Call WriIvl( -1, 'IDTask',IDTask )
      Select Case (iMod)
        Case (1)   ! KYF003
          Call KYFmodel003( IDTask, iMod, IsUndr, iStep, iTer, iEl, Int,
     *                   X, Y, Z, Time0, dTime,
     *                   Props, Sig0, Swp0, StVar0,
     *                   dEps, D, BulkW, Sig, Swp, StVar, ipl,
     *                   nStat, NonSym, iStrsDep, iTimeDep, iTang,
     *                   iAbort )
!        Case (2)   ! MC+Tension cut-off
!          Call MyMod_MC( IDTask, iMod, IsUndr, iStep, iTer, iEl, Int,
!     *                   X, Y, Z, Time0, dTime,
!     *                   Props, Sig0, Swp0, StVar0,
!     *                   dEps, D, BulkW, Sig, Swp, StVar, ipl,
!     *                   nStat, NonSym, iStrsDep, iTimeDep, iTang,
!     *                   iAbort )
!        Case (3)
!          Call MyModel3( IDTask, ....
        Case Default
          Write(1,*) 'invalid model number in UsrMod', iMod
          Write(1,*) 'IDTask: ',IDTask
          Stop 'invalid model number in UsrMod'
          iAbort=1
          Return
      End Select ! iMod
      If (IDTask .Eq. 5.And.iel+int.Eq.2) Then
        Write(1,*)'nStat   : ',nStat
        Write(1,*)'NonSym  : ',NonSym
        Write(1,*)'StrsDep : ',iStrsDep
        Write(1,*)'TimeDep : ',iTimeDep
        Write(1,*)'Tangent : ',iTang
      End If
      If (IDTask .Eq. 3.And.iel+int.Eq.2) Then
        Write(1,*)'IDTask: ',IDTask,' iStep,iTer',iStep,iTer
        Call Flush(1)
      End If
      Call WriIvl( -1, 'IDTask end',IDTask )
      Return
      End ! User_Mod
      include 'KYFmodel003.for'
!      include 'mymod_mc.for'
!      include 'mc_tens.for' ! used in mymod_mc
!      include 'usradddf.for' ! give parameter names and units
