### Plaxis UDSM

This is the code for a Plaxis User Defined Soil Models (UDSM), the model in this folder is an elastic-viscoplastic double-yield-surface model for coarse-grained soils. In another word, it is a time-dependent model that can describe the creep behavior of soils.  

The model can be named as `Hardening Soil Creep Model`, which is developed from the famous `Hardening Soil Model` and has considered creep.  

The codes are written in Fortran.  

### Introduction 

The proper modelling of time-dependent behavior, such as creep and strain rate effects, of coarse-grained soils (e.g., rockfills) is important to predict the long-term deformation of high-fill projects. In this paper, the primary features of coarse-grained soil are discussed, and an elastic-viscoplastic model is proposed for simulating its time-dependent behaviors in both shear and compression. The stress-dependence of the strength, dilatancy/contractibility and creep is carefully considered based on recent experimental findings. Verifications with various experimental results demonstrate that the model is capable of predicting the creep behavior in various stress states satisfactorily.  

### Keywords

- Constitutive model  
- Coarse-grained soil  
- Shear creep  
- Compression creep  

### Related Papers

If you find the code useful, please cite the paper below in your research paper:  

- Kong, Yufei, Ming Xu, and Erxiang Song. "[An elastic-viscoplastic double-yield-surface model for coarse-grained soils considering particle breakage](https://www.sciencedirect.com/science/article/pii/S0266352X16303147)." Computers and Geotechnics 85 (2017): 59-70.  


note : This is not necessarily the latest version of this constitutive model. The code uploaded here is merely for academic communication, commercial use not allowed.  
