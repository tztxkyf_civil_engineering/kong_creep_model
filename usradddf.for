! Subroutines in this file:
! Exported:
!  Subroutine GetModelCount( nMod )
!  Subroutine GetModelName ( iMod , ModelName )
!  Subroutine GetParamCount( iMod , nParam )
!  Subroutine GetParamName ( iMod , iParam, ParamName )
!  Subroutine GetParamUnit ( iMod , iParam, Units )
!  Subroutine GetStateVarCount( iMod , nVar )
!  Subroutine GetStateVarName ( iMod , iVar, Name )
!  Subroutine GetStateVarUnit ( iMod , iVar, Unit )
!
! Local:
!  Subroutine GetParamAndUnit( iMod , iParam, ParamName, Units )
!  Subroutine GetStateVarNameAndUnit( iMod , iVar, Name, Unit )



      Subroutine GetModelCount(nMod)
      !
      ! Return the maximum model number (iMod) in this DLL
      !
      Integer (Kind=4) nMod
!DEC$ ATTRIBUTES DLLExport, StdCall, reference ::  GetModelCount

      nMod = 1 ! Maximum model number (iMod) in current DLL

      Return
      End ! GetModelCount

      Subroutine GetModelName( iMod , ModelName )
      !
      ! Return the name of the different models
      !
      Integer  iMod
      Character (Len= * ) ModelName
      Character (Len=255) tName
!DEC$ ATTRIBUTES DLLExport, StdCall, reference ::  GetModelName

      Select Case (iMod)
        Case (1)
          tName = 'KYF003'
!        Case (2)
!          tName = 'MC'
        Case Default
          tName = 'not in DLL'
      End Select
      LT = Len_Trim(tName)
      ModelName= Char(lt) // tName(1:Lt)
      Return
      End ! GetModelName

      Subroutine GetParamCount( iMod , nParam )
      !
      ! Return the number of parameters of the different models
      !
!DEC$ ATTRIBUTES DLLExport, StdCall, reference ::  GetParamCount
      Select Case (iMod)
        Case ( 1 )
          nParam = 8
!        Case ( 2 )
!          nParam = 6
        Case Default
          nParam = 0
      End Select
      Return
      End ! GetParamCount

      Subroutine GetParamName( iMod , iParam, ParamName )
      !
      ! Return the parameters name of the different models
      !
      Character (Len=255) ParamName, Units
!DEC$ ATTRIBUTES DLLExport, StdCall, reference ::  GetParamName
      Call GetParamAndUnit(iMod,iParam,ParamName,Units)
      Return
      End

      Subroutine GetParamUnit( iMod , iParam, Units )
      !
      ! Return the units of the different parameters of the different models
      !
      Character (Len=255) ParamName, Units
!DEC$ ATTRIBUTES DLLExport, StdCall, reference ::  GetParamUnit
      Call GetParamAndUnit(iMod,iParam,ParamName,Units)
      Return
      End

      Subroutine GetParamAndUnit( iMod , iParam, ParamName, Units )
      !
      ! Return the parameters name and units of the different models
      !
      ! Units: use F for force unit
      !            L for length unit
      !            T for time unit
      !
      Character (Len=255) ParamName, Units, tName
      Select Case (iMod)
        Case (1)
          ! ModName = 'KYF003'
          Select Case (iParam)
            Case (1)
              ParamName = 'E'     ; Units     = 'F/L^2#'
            Case (2)
              ParamName = 'nu'    ; Units     = '-'
            Case (3)
              ParamName = 'C'     ; Units     = 'F/L^2#'
            Case (4)
              ParamName = 'Phi'   ; Units     = 'deg'
            Case (5)
              ParamName = 'Psi'   ; Units     = 'deg'
            Case (6)
              ParamName = 'Tens'  ; Units     = 'F/L^2#'  !above are basic parameters of MC model
            Case (7)
              ParamName = 'Beta(0~1)'     ; Units     = '-'
            Case (8)
              ParamName = 'c-visco(>0)'    ; Units     = '-'
            Case Default
              ParamName = '???'   ; Units     = '???'
          End Select
        Case Default
          ! model not in DLL
          ParamName = ' N/A '     ; Units     = ' N/A '
      End Select
      tName    = ParamName
      LT       = Len_Trim(tName)
      ParamName= Char(lt) // tName(1:Lt)

      tName = Units
      LT    = Len_Trim(tName)
      Units = Char(lt) // tName(1:Lt)

      Return
      End ! GetParamAndUnit


      Subroutine GetStateVarCount( iMod , nVar )
      !
      ! Return the number of state variables of the different models
      !

!DEC$ ATTRIBUTES dllexport, stdcall, reference :: GetStateVarCount

      	Select Case (iMod)
        Case (1)
          nVar = 0
        Case (2)
          nVar = 0
        Case Default
          nVar = 0
      End Select

      Return
      End


      Subroutine GetStateVarName( iMod , iVar, Name )
      !
      ! Return the name of the different state variables of the different models
      !
      Character (Len=255) Name, Unit

!DEC$ ATTRIBUTES dllexport, stdcall, reference :: GetStateVarName

        Call GetStateVarNameAndUnit( iMod , iVar, Name, Unit )

      End

      Subroutine GetStateVarUnit( iMod , iVar, Unit )
      !
      ! Return the units of the different state variables of the different models
      !
      Character (Len=255) Name, Unit

!DEC$ ATTRIBUTES dllexport, stdcall, reference :: GetStateVarUnit

        Call GetStateVarNameAndUnit( iMod , iVar, Name, Unit )
      End

      Subroutine GetStateVarNameAndUnit( iMod , iVar, Name, Unit )
      !
      ! Return the name and unit of the different state variables of the different models
      !
      Character (Len=255) Name, Unit, tName
      	Select Case (iMod)
        Case (1)
          Select Case (iVar)
            Case (1)
              Name = 'var_1#'         ; Unit = '-'
            Case (2)
              Name = 'var_2#'         ; Unit = '-'
            Case Default
              Name='N/A'              ; Unit = '?'
          End Select
        Case Default
          Name='N/A'                  ; Unit = '?'
      End Select
      tName    = Name
      LT       = Len_Trim(tName)
      Name     = Char(lt) // tName(1:Lt)

      tName    = Unit
      LT       = Len_Trim(tName)
      Unit     = Char(lt) // tName(1:Lt)
      Return
      End



