      Subroutine OK_MessageBox(t)
      use dfwin
      use dfwinty
      integer (kind=Int_Ptr_Kind()) hWnd

      character*(*) t

      character(len=256) :: msg, title

!  Display a messagebox with an OK button
!  Note that all strings must be null terminated for C's sake

      msg    = Trim(t)  // char(0)
      title = 'UDSM_Example' // char(0)

      hWnd = 0

      iret = MessageBox( hWnd,
     *                   msg,
     *                   title,
     *                   MB_OK )


      Return
      End
